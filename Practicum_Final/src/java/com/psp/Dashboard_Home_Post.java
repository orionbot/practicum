/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.psp;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpSession;


/**
 *
 * @author Aswin PC
 */
public class Dashboard_Home_Post extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
       
        HttpSession session=request.getSession(false);
        String name=new String();
        String assignment_type;
        
        
        if(session!=null)
           {
               name=(String)session.getAttribute("uname");
               request.setAttribute("uname",name);
           }  
        response.setContentType("text/html;charset=UTF-8");
        
        DateFormat mFormat = new SimpleDateFormat("MM/dd/yyyy");
        
        
        
        String assignment_name=request.getParameter("Assignment_Name_input");
        String description_input=request.getParameter("Description_input");
        
        String date_input=request.getParameter("Date_input");
        assignment_type=request.getParameter("assignment_type");
        
        
        java.util.Date date;
        java.sql.Date sqlStartDate;
        java.sql.Date AssignmentDate;
        
        try
        { 
           
           
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        request.setAttribute("assignment_name",assignment_name);
        session.setAttribute("assignment_name",assignment_name);
        request.setAttribute("description_input",description_input);
        request.setAttribute("date_input",date_input);
        request.setAttribute("assignment_type",assignment_type);
        
        try
        {
                         date=mFormat.parse(date_input);
                         sqlStartDate= new java.sql.Date(date.getTime()); 
                         
                        Class.forName("com.mysql.jdbc.Driver");
                        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/psptool","root","");
                        System.out.println("Connected with Database successfully bro!!");
                        String insertTableSQL = "INSERT INTO assignments"
		+ "(studentname, assignmentname,assignmenttype,description,creation_date) values"
		+ "(?,?,?,?,?)";
                        
                        PreparedStatement preparedStatement = con.prepareStatement(insertTableSQL);
                        preparedStatement.setString(1, name);
                        preparedStatement.setString(2, assignment_name);
                        preparedStatement.setString(3, assignment_type);
                        preparedStatement.setString(4, description_input);
                        preparedStatement.setDate(5,sqlStartDate);
                        
                        preparedStatement.executeUpdate();

                        System.out.println("Inserted row successfully");
                        
                        
                        String TimeRecordTableSQL1 = "INSERT INTO timerecordform"
		+ "(studentname, assignmentname,phase) values"
		+ "(?,?,?)";
                        
                        PreparedStatement preparedStatement2 = con.prepareStatement(TimeRecordTableSQL1);
                        preparedStatement2.setString(1, name);
                        preparedStatement2.setString(2, assignment_name);
                        preparedStatement2.setString(3, "Plan");
                        
                        preparedStatement2.executeUpdate();

                        System.out.println("Inserted row successfully into Time Record Form Table");
                        
                      
                        PreparedStatement preparedStatement3 = con.prepareStatement(TimeRecordTableSQL1);
                        preparedStatement3.setString(1, name);
                        preparedStatement3.setString(2, assignment_name);
                        preparedStatement3.setString(3, "Design");
                        
                        preparedStatement3.executeUpdate();

                        System.out.println("Inserted row successfully into Time Record Form Table");
            
            
                        
                        PreparedStatement preparedStatement4 = con.prepareStatement(TimeRecordTableSQL1);
                        preparedStatement4.setString(1, name);
                        preparedStatement4.setString(2, assignment_name);
                        preparedStatement4.setString(3, "Code");
                        
                        preparedStatement4.executeUpdate();

                        System.out.println("Inserted row successfully into Time Record Form Table");
                        
                        PreparedStatement preparedStatement5 = con.prepareStatement(TimeRecordTableSQL1);
                        preparedStatement5.setString(1, name);
                        preparedStatement5.setString(2, assignment_name);
                        preparedStatement5.setString(3, "Test");
                        
                        preparedStatement5.executeUpdate();

                        System.out.println("Inserted row successfully into Time Record Form Table");
                        
                        PreparedStatement preparedStatement6 = con.prepareStatement(TimeRecordTableSQL1);
                        preparedStatement6.setString(1, name);
                        preparedStatement6.setString(2, assignment_name);
                        preparedStatement6.setString(3, "PostMortem");
                        
                        preparedStatement6.executeUpdate();

                        System.out.println("Inserted row successfully into Time Record Form Table");
        }   
        
         catch(Exception ex)
         {
                      System.out.println(ex.getMessage());
          }    
        
        RequestDispatcher rd= request.getRequestDispatcher("pages/Assignment_Home.jsp");
        rd.forward(request,response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

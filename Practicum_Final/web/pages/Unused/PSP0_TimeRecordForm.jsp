<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PSP 0 Time Record Log</title>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> 
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    
    <!-- JQUERY LIBRRAY IMPORT-->

    <!-- Bootstrap Core CSS -->
    <link href="./vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
    <link href="./dist/css/PSP0_TimeRecordForm.css" rel="stylesheet">
    

    <!-- MetisMenu CSS -->
    <link href="./vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="./vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="./vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    

</head>

<script type="text/javascript">
   $(document).ready(function() {
       
      
    
       
  $("#Timerecordform").on('submit',(function(e) {
      
      e.preventDefault();
      
          $.ajax({
            url: 'PSP0_Time_2',
            type: 'POST',
            data: {
                a1:document.getElementsByName('a1')[0].value,
                a2:document.getElementsByName('a2')[0].value,
                a3:document.getElementsByName('a3')[0].value,
                a4:document.getElementsByName('a4')[0].value,
                a5:document.getElementsByName('a5')[0].value,
                a6:document.getElementsByName('a6')[0].value,
                
                b1:document.getElementsByName('b1')[0].value,
                b2:document.getElementsByName('b2')[0].value,
                b3:document.getElementsByName('b3')[0].value,
                b4:document.getElementsByName('b4')[0].value,
                b5:document.getElementsByName('b5')[0].value,
                b6:document.getElementsByName('b6')[0].value,
                
                c1:document.getElementsByName('c1')[0].value,
                c2:document.getElementsByName('c2')[0].value,
                c3:document.getElementsByName('c3')[0].value,
                c4:document.getElementsByName('c4')[0].value,
                c5:document.getElementsByName('c5')[0].value,
                c6:document.getElementsByName('c6')[0].value,
                
                d1:document.getElementsByName('d1')[0].value,
                d2:document.getElementsByName('d2')[0].value,
                d3:document.getElementsByName('d3')[0].value,
                d4:document.getElementsByName('d4')[0].value,
                d5:document.getElementsByName('d5')[0].value,
                d6:document.getElementsByName('d6')[0].value,
                
                e1:document.getElementsByName('e1')[0].value,
                e2:document.getElementsByName('e2')[0].value,
                e3:document.getElementsByName('e3')[0].value,
                e4:document.getElementsByName('e4')[0].value,
                e5:document.getElementsByName('e5')[0].value,
                e6:document.getElementsByName('e6')[0].value

                
                



               
               
                   

     },
            success: function(data) {
                alert('Your Form data has been saved to the Database');
            },
            error: function(data) {
                alert('Form data not saved to the database');
            }
        });
    }));  
});
</script>    

<!--<script>
    
    $(document).ready(function() { 
    
    var form_data=JSON.parse('${student_data}');
    
    document.getElementsByName('a1')[0].value=form_data[2].phase;
    document.getElementsByName('a2')[0].value=form_data[2].plannedTime;
    document.getElementsByName('a3')[0].value=parseFloat(form_data[2].actual_startTime).toFixed(2);
    document.getElementsByName('a4')[0].value=parseFloat(form_data[2].actual_stopTime).toFixed(2);
    document.getElementsByName('a5')[0].value=form_data[2].interruptionTime;
    document.getElementsByName('a6')[0].value=form_data[2].comments;
    
    document.getElementsByName('b1')[0].value=form_data[1].phase;
    document.getElementsByName('b2')[0].value=form_data[1].plannedTime;
    document.getElementsByName('b3')[0].value=form_data[1].actual_startTime.toFixed(2);
    document.getElementsByName('b4')[0].value=form_data[1].actual_stopTime.toFixed(2);
    document.getElementsByName('b5')[0].value=form_data[1].interruptionTime;
    document.getElementsByName('b6')[0].value=form_data[1].comments;
    
    document.getElementsByName('c1')[0].value=form_data[0].phase;
    document.getElementsByName('c2')[0].value=form_data[0].plannedTime;
    document.getElementsByName('c3')[0].value=parseFloat(form_data[0].actual_startTime).toFixed(2);
    document.getElementsByName('c4')[0].value=parseFloat(form_data[0].actual_stopTime).toFixed(2);
    document.getElementsByName('c5')[0].value=form_data[0].interruptionTime;
    document.getElementsByName('c6')[0].value=form_data[0].comments;
    
    document.getElementsByName('d1')[0].value=form_data[4].phase;
    document.getElementsByName('d2')[0].value=form_data[4].plannedTime;
    document.getElementsByName('d3')[0].value=form_data[4].actual_startTime.toFixed(2);
    document.getElementsByName('d4')[0].value=form_data[4].actual_stopTime.toFixed(2);
    document.getElementsByName('d5')[0].value=form_data[4].interruptionTime;
    document.getElementsByName('d6')[0].value=form_data[4].comments;
    
    
    document.getElementsByName('e1')[0].value=form_data[3].phase;
    document.getElementsByName('e2')[0].value=form_data[3].plannedTime;
    document.getElementsByName('e3')[0].value=form_data[3].actual_startTime.toFixed(2);
    document.getElementsByName('e4')[0].value=form_data[3].actual_stopTime.toFixed(2);
    document.getElementsByName('e5')[0].value=form_data[3].interruptionTime;
    document.getElementsByName('e6')[0].value=form_data[3].comments;
    
    
});
</script>    -->
            
            

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="Dashboard_Home_Pre">PSP Dashboard</a>
            </div>
            <!-- /.navbar-header -->

            
              
                
                <div id="username_div">
               
                <label><strong> Hi, <%= session.getAttribute("uname").toString() %> </strong></label>
                
                 
                <label id="LogOutLabel"><a href="./LogOut"><i class="fa fa-sign-out fa-fw"></i> Logout</a> </label>
                
                </div>
                <!-- /.dropdown -->
            
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                        
                        <li>
                            <a href="#"><i class="fa fa-file fa-fw"> </i> Forms<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="pages/PSP0_Defect.html">Defect Record</a>
                                </li>
                                <li>
                                    <a href="pages/PSP_LOC_Actual.html">LOC Actual</a>
                                </li>
                                <li>
                                    <a href="pages/PSP_LOC_Planned.html">LOC Planned</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                    </li>
                        
                        <li>
                            <a href="#"><i class="fa fa-bar-chart fa-fw"></i> Visualizations <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="pages/PSP0_TimeRecordForm_Visualization.jsp">Planned vs Actual Time </a>
                                </li>
                                <li>
                                    <a href="#">Defects injected per phase</a>
                                </li>
                               
                            </ul>
                           
                        </li>
                        </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

   
                  
                
                <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                      <h1 class="page-header"><%= session.getAttribute("assignment_name").toString() %>-Time Record Form</h1>
    
                
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Please enter your data.
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            
                            
                            
                            <form id="Timerecordform">
                                
                                
                                
                                <table width="100%" class="table table-striped table-bordered table-hover"  id="Time_Record_Table-example">
                                
                                <thead>
                                    <tr>
                                        <th>Phase</th>
                                        <th>Planned Time (Mins)</th>
                                        <th>Actual Start Time</th>
                                        <th>Actual Stop time</th>
                                        <th>Interruption Time (Mins)</th>
                                        <th>Comments</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX" >
                                    
                                    
                                        
                                        <td>
                                            <input type="text" name="a1" id="Phase" value="Plan" readonly>

                                        </td>
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty Plan_plantime}">
                                       <input type="number" name="a2" id="PlannedTime">
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number" name="a2" id="PlannedTime" value="${Plan_plantime}"> 
                                       </c:otherwise>
                                     </c:choose>
                                            

                                        </td> 
                                      
                                        <td>
                                        <c:choose>
                                        <c:when test="${empty Plan_starttime}">
                                       <input type="number" step="0.01" name="a3" id="Actual_startTime" >
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number" step="0.01" name="a3" id="Actual_startTime" value="${Plan_starttime}" >
                                         
                                       </c:otherwise>
                                     </c:choose>
                                         
                                       
                                             

                                        </td>
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty Plan_stoptime}">
                                       <input type="number" step="0.01" name="a4" id="Actual_stopTime">
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number" step="0.01" name="a4" id="Actual_stopTime" value="${Plan_stoptime}" >
                                       </c:otherwise>
                                     </c:choose>
                                             
                                        </td>
                                        
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty Plan_interruptionTime}">
                                       <input type="number"  name="a5" id="Interruption_time" >
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number"  name="a5" id="Interruption_time" value="${Plan_interruptionTime}" >
                                       </c:otherwise>
                                     </c:choose>
                                            
                                        </td>
                                        
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty Plan_comments}">
                                       <input type="text" name="a6" id="Comments">
                                       </c:when>
                                       <c:otherwise>
                                         <input type="text" name="a6" id="Comments" value="${Plan_comments}" >
                                       </c:otherwise>
                                     </c:choose>
                                            
                                        </td>
                                        
                                    </tr>
                                    
                                    <tr class="odd gradeX" >
                                        
                                        <td>
                                             <input type="text" name="b1" id="Phase" value="Design" readonly  >

                                        </td>
                                        
                                       										<td>
                                            <c:choose>
                                        <c:when test="${empty Design_plantime}">
                                       <input type="number" name="b2" id="DesignnedTime">
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number" name="b2" id="DesignnedTime" value="${Design_plantime}"> 
                                       </c:otherwise>
                                     </c:choose>
                                            

                                        </td>
                                      
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty Design_starttime}">
                                       <input type="number" step="0.01" name="b3" id="Actual_startTime" >
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number" step="0.01" name="b3" id="Actual_startTime" value="${Design_starttime}" >
                                       </c:otherwise>
                                     </c:choose>
                                             
                                             

                                        </td>
                                         <td>
                                            <c:choose>
                                        <c:when test="${empty Design_stoptime}">
                                       <input type="number" step="0.01" name="b4" id="Actual_stopTime">
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number" step="0.01" name="b4" id="Actual_stopTime" value="${Design_stoptime}" >
                                       </c:otherwise>
                                     </c:choose>
                                             
                                        </td>
                                        
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty Design_interruptionTime}">
                                       <input type="number"  name="b5" id="Interruption_time" >
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number"  name="b5" id="Interruption_time" value="${Design_interruptionTime}" >
                                       </c:otherwise>
                                     </c:choose>
                                            
                                        </td>
                                        
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty Design_comments}">
                                       <input type="text" name="b6" id="Comments">
                                       </c:when>
                                       <c:otherwise>
                                         <input type="text" name="b6" id="Comments" value="${Design_comments}" >
                                       </c:otherwise>
                                     </c:choose>
                                            
                                        </td>
                                        
                                    </tr>
                                    
                                      <tr class="odd gradeX" >
                                        
                                        <td>
                                            <input type="text" name="c1" id="Phase" value="Code" readonly>

                                        </td>
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty Code_plantime}">
                                       <input type="number" name="c2" id="PlannedTime">
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number" name="c2" id="CodenedTime" value="${Code_plantime}"> 
                                       </c:otherwise>
                                     </c:choose>
                                            

                                        </td> 
                                      
                                        <td>
                                         <c:choose>
                                        <c:when test="${empty Code_starttime}">
                                       <input type="number" step="0.01" name="c3" id="Actual_startTime" >
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number" step="0.01" name="c3" id="Actual_startTime" value="${Code_starttime}" >
                                       </c:otherwise>
                                     </c:choose>
                                         
                                       
                                             

                                        </td>
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty Code_stoptime}">
                                       <input type="number" step="0.01" name="c4" id="Actual_stopTime">
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number" step="0.01" name="c4" id="Actual_stopTime" value="${Code_stoptime}" >
                                       </c:otherwise>
                                     </c:choose>
                                             
                                        </td>
                                        
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty Code_interruptionTime}">
                                       <input type="number"  name="c5" id="Interruption_time" >
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number"  name="c5" id="Interruption_time" value="${Code_interruptionTime}" >
                                       </c:otherwise>
                                     </c:choose>
                                            
                                        </td>
                                        
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty Code_comments}">
                                       <input type="text" name="c6" id="Comments">
                                       </c:when>
                                       <c:otherwise>
                                         <input type="text" name="c6" id="Comments" value="${Code_comments}" >
                                       </c:otherwise>
                                     </c:choose>
                                            
                                        </td>
                                        
                                    </tr>
                                    
                                     <tr class="odd gradeX" >
                                        
                                        <td>
                                            <input type="text" name="d1" id="Phase" value="Test" readonly>

                                        </td>
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty Test_plantime}">
                                       <input type="number" name="d2" id="PlannedTime">
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number" name="d2" id="TestnedTime" value="${Test_plantime}"> 
                                       </c:otherwise>
                                     </c:choose>
                                            

                                        </td> 
                                      
                                        <td>
                                         <c:choose>
                                        <c:when test="${empty Test_starttime}">
                                       <input type="number" step="0.01" name="d3" id="Actual_startTime" >
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number" step="0.01" name="d3" id="Actual_startTime" value="${Test_starttime}" >
                                       </c:otherwise>
                                     </c:choose>
                                         
                                       
                                             

                                        </td>
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty Test_stoptime}">
                                       <input type="number" step="0.01" name="d4" id="Actual_stopTime">
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number" step="0.01" name="d4" id="Actual_stopTime" value="${Test_stoptime}" >
                                       </c:otherwise>
                                     </c:choose>
                                             
                                        </td>
                                        
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty Test_interruptionTime}">
                                       <input type="number"  name="d5" id="Interruption_time" >
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number"  name="d5" id="Interruption_time" value="${Test_interruptionTime}" >
                                       </c:otherwise>
                                     </c:choose>
                                            
                                        </td>
                                        
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty Test_comments}">
                                       <input type="text" name="d6" id="Comments">
                                       </c:when>
                                       <c:otherwise>
                                         <input type="text" name="d6" id="Comments" value="${Test_comments}" >
                                       </c:otherwise>
                                     </c:choose>
                                            
                                        </td>
                                        
                                    </tr>
                                    
                                     <tr class="odd gradeX" >
                                        
                                        <td>
                                            <input type="text" name="e1" id="Phase" value="PostMortem" readonly>

                                        </td>
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty PostMortem_plantime}">
                                       <input type="number" name="e2" id="PlannedTime">
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number" name="e2" id="PostMortemnedTime" value="${PostMortem_plantime}"> 
                                       </c:otherwise>
                                     </c:choose>
                                            

                                        </td> 
                                      
                                        <td>
                                         <c:choose>
                                        <c:when test="${empty PostMortem_starttime}">
                                       <input type="number" step="0.01" name="e3" id="Actual_startTime" >
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number" step="0.01" name="e3" id="Actual_startTime" value="${PostMortem_starttime}" >
                                       </c:otherwise>
                                     </c:choose>
                                         
                                       
                                             

                                        </td>
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty PostMortem_stoptime}">
                                       <input type="number" step="0.01" name="e4" id="Actual_stopTime">
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number" step="0.01" name="e4" id="Actual_stopTime" value="${PostMortem_stoptime}" >
                                       </c:otherwise>
                                     </c:choose>
                                             
                                        </td>
                                        
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty PostMortem_interruptionTime}">
                                       <input type="number"  name="e5" id="Interruption_time" >
                                       </c:when>
                                       <c:otherwise>
                                         <input type="number"  name="e5" id="Interruption_time" value="${PostMortem_interruptionTime}" >
                                       </c:otherwise>
                                     </c:choose>
                                            
                                        </td>
                                        
                                        <td>
                                            <c:choose>
                                        <c:when test="${empty PostMortem_comments}">
                                       <input type="text" name="e6" id="Comments">
                                       </c:when>
                                       <c:otherwise>
                                         <input type="text" name="e6" id="Comments" value="${PostMortem_comments}" >
                                       </c:otherwise>
                                     </c:choose>
                                            
                                        </td>
                                        
                                    </tr>
                                </tbody>
                                
                                </table>
                                
                                <br/><button type="submit" id="Submit" value="Submit" class="btn btn-lg btn-success btn-block" style="width:150px;"> Save </button>
                            
                           
                            </form>
                              
                           
                        </div>
               
                           

                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
           

    
              

    
    
    
            </div>
            </div> 
    </div>          
                      


    <!-- Bootstrap Core JavaScript -->
    <script src="./vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="./vendor/metisMenu/metisMenu.min.js"></script>

    

    <!-- Custom Theme JavaScript -->
    <script src="./dist/js/sb-admin-2.js"></script>
        
        
</body>

</html>


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.psp.NotUsed;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Aswin PC
 */
public class PSP0_Time_Update extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
                
        PrintWriter out = response.getWriter();
        HttpSession session=request.getSession(false);

        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
                HttpSession session=request.getSession(false);
                
                
                if(session!=null)
              {
                  try
                  {
                        Class.forName("com.mysql.jdbc.Driver");
                        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/psptool","root","password");
                        System.out.println("Connected with Database successfully bro!!");
                        
                        String updateTableSQL = "UPDATE timerecordform"
		+ " SET phase=?,plantime=?,actual_starttime=?,actual_stoptime=?,actualtime=?,interruptionTime=?,comments=? where studentname=? AND (assignmentname=? AND phase=?)";
		
                        
                        String assignmentname=session.getAttribute("assignment_name").toString();
                        
                        
                        
                        JSONArray phase_data = new JSONArray();
                        
                        
                        
                        
                        
                        System.out.println("Parameters for:"+session.getAttribute("uname"));
                        
                        String name=(String)session.getAttribute("uname");
                        String phase=(String)request.getParameter("1a");
                        int plannedTime=Integer.parseInt(request.getParameter("2a"));
                        float startTime=Float.parseFloat(request.getParameter("3a"));
                        float stopTime=Float.parseFloat(request.getParameter("4a"));
                        int interruptionTime=Integer.parseInt(request.getParameter("5a"));
                        int actualTime=(int)((stopTime-startTime)*60)-interruptionTime;
                        
                        String comments=request.getParameter("6a");
                        
                        JSONObject phase1=new JSONObject();
                        phase1.put("phase", phase);
                        phase1.put("plannedTime", plannedTime);
                        phase1.put("actualTime", actualTime);
                        
                        phase_data.put(phase1);
                        
                        
                        System.out.println(phase+" Phase");

                        System.out.println("plannedTime:"+plannedTime);
                        System.out.println("startTime:"+startTime);
                        System.out.println("stopTime:"+stopTime);
                        System.out.println("InterruptionTime:"+interruptionTime);
                        System.out.println("Phase:"+phase);
                        System.out.println("Comments:"+comments);

                        
                        
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setString(1, phase);
                        preparedStatement.setInt (2, plannedTime);
                        preparedStatement.setFloat(3, startTime);
                        preparedStatement.setFloat(4, stopTime);
                        preparedStatement.setInt(5, actualTime);
                        preparedStatement.setInt(6, interruptionTime);
                        preparedStatement.setString(7, comments);
                        preparedStatement.setString(8, name);
                        preparedStatement.setString(9, assignmentname);
                        preparedStatement.setString(10, phase);
                        // execute insert SQL stetement
                        preparedStatement .executeUpdate();
                        
                       
                        
                        System.out.println("Inserted a row successfully");

                        
                        String phase_2=(String)request.getParameter("1b");
                        int plannedTime_2=Integer.parseInt(request.getParameter("2b"));
                        float startTime_2=Float.parseFloat(request.getParameter("3b"));
                        float stopTime_2=Float.parseFloat(request.getParameter("4b"));
                        int interruptionTime_2=Integer.parseInt(request.getParameter("5b"));
                        int actualTime_2=(int)((stopTime_2-startTime_2)*60)-interruptionTime_2;
                        
                        String comments_2=(String)request.getParameter("6b");

                        System.out.println(phase_2+" phase");
                        System.out.println("plannedTime_2:"+plannedTime_2);
                        System.out.println("startTime_2:"+startTime_2);
                        System.out.println("stopTime_2:"+stopTime_2);
                        System.out.println("interruptionTime_2:"+interruptionTime_2);
                        System.out.println("Work Done:"+comments_2);
                        
                        
                        JSONObject phase2=new JSONObject();
                        phase2.put("phase", phase_2);
                        phase2.put("plannedTime", plannedTime_2);
                        phase2.put("actualTime", actualTime_2);
                        
                        phase_data.put(phase2);
                        
                        
                        
                        PreparedStatement preparedStatement2 = con.prepareStatement(updateTableSQL);
                        preparedStatement2.setString(1, phase_2);
                        preparedStatement2.setInt (2, plannedTime_2);
                        preparedStatement2.setFloat(3, startTime_2);
                        preparedStatement2.setFloat(4, stopTime_2);
                        preparedStatement2.setInt(5, actualTime_2);
                        preparedStatement2.setInt(6, interruptionTime_2);
                        preparedStatement2.setString(7, comments_2);
                        preparedStatement2.setString(8, name);
                        preparedStatement2.setString(9, assignmentname);
                        preparedStatement2.setString(10, phase_2);
                        // execute insert SQL statement
                        preparedStatement2.executeUpdate();

                        System.out.println("Inserted another row successfully");
                        
                        
                        String phase_3=(String)request.getParameter("1c");
                        int plannedTime_3=Integer.parseInt(request.getParameter("2c"));
                        float startTime_3=Float.parseFloat(request.getParameter("3c"));
                        float stopTime_3=Float.parseFloat(request.getParameter("4c"));
                        int interruptionTime_3=Integer.parseInt(request.getParameter("5c"));
                        int actualTime_3=(int)((stopTime_3-startTime_3)*60)-(interruptionTime_3);
                        
                        String comments_3=(String)request.getParameter("6c");

                        System.out.println(phase_3+" phase");
                        System.out.println("plannedTime_3:"+plannedTime_3);
                        System.out.println("startTime_3:"+startTime_3);
                        System.out.println("stopTime_3:"+stopTime_3);
                        System.out.println("interruptionTime_3:"+interruptionTime_3);
                        System.out.println("Work Done:"+comments_3);
                        
                        
                        JSONObject phase3=new JSONObject();
                        phase3.put("phase", phase_3);
                        phase3.put("plannedTime", plannedTime_3);
                        phase3.put("actualTime", actualTime_3);
                        
                        phase_data.put(phase3);
                        
                        
                        
                        PreparedStatement preparedStatement3 = con.prepareStatement(updateTableSQL);
                        preparedStatement3.setString(1, phase_3);
                        preparedStatement3.setInt (2, plannedTime_3);
                        preparedStatement3.setFloat(3, startTime_3);
                        preparedStatement3.setFloat(4, stopTime_3);
                        preparedStatement3.setInt(5, actualTime_3);
                        preparedStatement3.setInt(6, interruptionTime_3);
                        preparedStatement3.setString(7, comments_3);
                        preparedStatement3.setString(8, name);
                        preparedStatement3.setString(9, assignmentname);
                        preparedStatement3.setString(10, phase_3);
						
						
                        // execute insert SQL stetement
                        preparedStatement3.executeUpdate();

                        System.out.println("Inserted another row successfully");
                        
           	        String phase_4=(String)request.getParameter("1d");
                        int plannedTime_4=Integer.parseInt(request.getParameter("2d"));
                        float startTime_4=Float.parseFloat(request.getParameter("3d"));
                        float stopTime_4=Float.parseFloat(request.getParameter("4d"));
                        int interruptionTime_4=Integer.parseInt(request.getParameter("5d"));
                        int actualTime_4=(int)((stopTime_4-startTime_4)*60)-interruptionTime_4;
                        
                        String comments_4=(String)request.getParameter("6d");

                        System.out.println(phase_4+" phase");
                        System.out.println("plannedTime_4:"+plannedTime_4);
                        System.out.println("startTime_4:"+startTime_4);
                        System.out.println("stopTime_4:"+stopTime_4);
                        System.out.println("interruptionTime_4:"+interruptionTime_4);
                        System.out.println("Work Done:"+comments_4);
                        
                        
                        JSONObject phase4=new JSONObject();
                        phase4.put("phase", phase_4);
                        phase4.put("plannedTime", plannedTime_4);
                        phase4.put("actualTime", actualTime_4);
                        
                        phase_data.put(phase4);
                        
                        
                        
                        PreparedStatement preparedStatement4 = con.prepareStatement(updateTableSQL);
                        preparedStatement4.setString(1, phase_4);
                        preparedStatement4.setInt (2, plannedTime_4);
                        preparedStatement4.setFloat(3, startTime_4);
                        preparedStatement4.setFloat(4, stopTime_4);
                        preparedStatement4.setInt(5, actualTime_4);
                        preparedStatement4.setInt(6, interruptionTime_4);
                        preparedStatement4.setString(7, comments_4);
                        preparedStatement4.setString(8, name);
                        preparedStatement4.setString(9, assignmentname);
                        preparedStatement4.setString(10, phase_4);
						
						
                        // execute insert SQL stetement
                        preparedStatement4.executeUpdate();

                        System.out.println("Inserted another row successfully");

                        String phase_5=(String)request.getParameter("1e");
                        int plannedTime_5=Integer.parseInt(request.getParameter("2e"));
                        float startTime_5=Float.parseFloat(request.getParameter("3e"));
                        float stopTime_5=Float.parseFloat(request.getParameter("4e"));
                        int interruptionTime_5=Integer.parseInt(request.getParameter("5e"));
                        int actualTime_5=(int)((stopTime_5-startTime_5)*60)-interruptionTime_5;
                        
                        String comments_5=(String)request.getParameter("6e");

                        System.out.println(phase_5+" phase");
                        System.out.println("plannedTime_5:"+plannedTime_5);
                        System.out.println("startTime_5:"+startTime_5);
                        System.out.println("stopTime_5:"+stopTime_5);
                        System.out.println("interruptionTime_5:"+interruptionTime_5);
                        System.out.println("Work Done:"+comments_5);
                        
                        
                        JSONObject phase5=new JSONObject();
                        phase5.put("phase", phase_5);
                        phase5.put("plannedTime", plannedTime_5);
                        phase5.put("actualTime", actualTime_5);
                        
                        phase_data.put(phase5);
                        
                        
                        
                        PreparedStatement preparedStatement5 = con.prepareStatement(updateTableSQL);
                        
                        preparedStatement5.setString(1, phase_5);
                        preparedStatement5.setInt (2, plannedTime_5);
                        preparedStatement5.setFloat(3, startTime_5);
                        preparedStatement5.setFloat(4, stopTime_5);
                        preparedStatement5.setInt(5, actualTime_5);
                        preparedStatement5.setInt(6, interruptionTime_5);
                        preparedStatement5.setString(7, comments_5);
                        preparedStatement5.setString(8, name);
                        preparedStatement5.setString(9, assignmentname);
                        preparedStatement5.setString(10, phase_5);
						
						
                        
                        // execute insert SQL stetement
                        preparedStatement5.executeUpdate();

                        System.out.println("Inserted another row successfully");
                        
                        String responsetojsp=phase_data.toString();
                        request.setAttribute("mainjson",responsetojsp);
                        request.setAttribute("uname",name);
                            
                                                     
                            System.out.println(responsetojsp);
                            System.out.println("Going to forward request to visualization page");
                            
                            RequestDispatcher dispatcher=request.getRequestDispatcher("pages/Time_Visualization_2.jsp");
                            dispatcher.forward(request,response);
                            
                            
                                               
                            con.close();
                        
                        
               

                       }
                  
                  catch(ClassNotFoundException | NumberFormatException | SQLException ex)
                  {
                      System.out.println(ex.getMessage());
                  }    
               
    }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//just to make a small change and push to master
package com.psp;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Aswin PC
 */
public class PSP0_TimeRecord_Visualize extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
         response.setContentType("text/html;charset=UTF-8");
         
         String responsetoJsp=new String();
        
        Connection con;
        ResultSet rs;
        DecimalFormat df=new DecimalFormat("#.##");
        boolean planFlag=false,designFlag=false,codeFlag=false,testFlag=false,postmortemFlag=false;
        
        
        try{
       
        Class.forName("com.mysql.jdbc.Driver");
        con=DriverManager.getConnection("jdbc:mysql://localhost:3306/psptool","root","");
        System.out.println("Connected with Database successfully!!");
        Statement stmt=con.createStatement();
        HttpSession session=request.getSession(false);
        String username=new String();
        JSONArray phase_data = new JSONArray();

        String selected_assignment = request.getParameter("assignment") ;
        
        System.out.println("Assignment name selected for Visualization:"+selected_assignment);
        
            if(session!=null)
           {
               username=(String)session.getAttribute("uname");
           }
            
        rs=stmt.executeQuery("select plantime,actualtime,actual_starttime,actual_stoptime,interruptionTime,comments from timerecordform where studentname='"+username+"' and assignmentname='"+selected_assignment+"' and phase='Plan'");
        
        while(rs.next())
        {   
            if(rs.getInt("actualtime")!=0)
            {
                int plantime=rs.getInt("plantime");
                int actualtime=rs.getInt("actualtime");
                JSONObject phase1=new JSONObject();
                phase1.put("phase", "Plan");
                phase1.put("plannedTime", plantime);
                phase1.put("actualTime", actualtime);
                phase_data.put(phase1);
                        
            }
            else
                planFlag=true;
        }
            
        
        
        // Fetching Values for Design Phase
        
        rs=stmt.executeQuery("select plantime,actualtime,actual_starttime,actual_stoptime,interruptionTime,comments from timerecordform where studentname='"+username+"' and assignmentname='"+selected_assignment+"' and phase='Design'");
        
        while(rs.next() && !planFlag)
        {   
            if(rs.getInt("actualtime")!=0)
            {
                int plantime=rs.getInt("plantime");
                int actualtime=rs.getInt("actualtime");
                JSONObject phase1=new JSONObject();
                phase1.put("phase", "Plan");
                phase1.put("plannedTime", plantime);
                phase1.put("actualTime", actualtime);
                phase_data.put(phase1);
                        
            }
            else
                designFlag=true;
            
           
            
        }
        
        rs=stmt.executeQuery("select plantime,actualtime,actual_starttime,actual_stoptime,interruptionTime,comments from timerecordform where studentname='"+username+"' and assignmentname='"+selected_assignment+"' and phase='Code'");
        
        while(rs.next()&& !planFlag && !designFlag)
        {   
             if(rs.getInt("actualtime")!=0)
            {
                int plantime=rs.getInt("plantime");
                int actualtime=rs.getInt("actualtime");
                JSONObject phase1=new JSONObject();
                phase1.put("phase", "Plan");
                phase1.put("plannedTime", plantime);
                phase1.put("actualTime", actualtime);
                phase_data.put(phase1);
                        
            }
            else
                codeFlag=true;
            
        }
        
        rs=stmt.executeQuery("select plantime,actualtime,actual_starttime,actual_stoptime,interruptionTime,comments from timerecordform where studentname='"+username+"' and assignmentname='"+selected_assignment+"' and phase='Test'");
        
        while(rs.next() && !planFlag && !designFlag && !codeFlag)
        {   
            if(rs.getInt("actualtime")!=0)
            {
                int plantime=rs.getInt("plantime");
                int actualtime=rs.getInt("actualtime");
                JSONObject phase1=new JSONObject();
                phase1.put("phase", "Plan");
                phase1.put("plannedTime", plantime);
                phase1.put("actualTime", actualtime);
                phase_data.put(phase1);
                        
            }
            else
                designFlag=true;
            
            
            
        }
        
        rs=stmt.executeQuery("select plantime,actualtime,actual_starttime,actual_stoptime,interruptionTime,comments from timerecordform where studentname='"+username+"' and assignmentname='"+selected_assignment+"' and phase='PostMortem'");
        
        while(rs.next() && !planFlag && !designFlag && !codeFlag && !designFlag)
        {   
           if(rs.getInt("actualtime")!=0)
            {
                int plantime=rs.getInt("plantime");
                int actualtime=rs.getInt("actualtime");
                JSONObject phase1=new JSONObject();
                phase1.put("phase", "Plan");
                phase1.put("plannedTime", plantime);
                phase1.put("actualTime", actualtime);
                phase_data.put(phase1);
                        
            }
            else
                postmortemFlag=true;
            
            
            
        }
        
        if(planFlag || designFlag || codeFlag || designFlag || postmortemFlag)
        {
            responsetoJsp="The form for the Assignment has not been filled yet!";
            response.getWriter().write(responsetoJsp);
        }    
        
        else
            
        {
            responsetoJsp=phase_data.toString();
            response.getWriter().write(responsetoJsp);
            
        } 
        
        
        
        System.out.println(responsetoJsp);
        //RequestDispatcher rd= request.getRequestDispatcher("pages/PSP0_TimeRecordForm_Visualization.jsp");
         
        //System.out.println("Executed till this point");
        //rd.forward(request,response);
         
        
        
         
         System.out.println("No Exception till this point");
            
        }
         catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }  
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

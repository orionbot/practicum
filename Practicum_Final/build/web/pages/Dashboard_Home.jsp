<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard Home</title>
    
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
    
    
    

    <!-- jQuery -->
    <script src="./vendor/jquery/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    
    
    
    <!-- Bootstrap Core CSS -->
    <link href="./vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    

    <!-- MetisMenu CSS -->
    <link href="./vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./dist/css/sb-admin-2.css" rel="stylesheet">
    
    <link href="./dist/css/Dashboard_Home.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="./vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="./vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    
    
    

</head>

<script>
    $(function() {
        $("#Date_input").datepicker();
    });
</script>    

<body>
    
   

    

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">PSP Dashboard</a>
                
                
            </div>
            <!-- /.navbar-header -->
            
            <div id="username_div">
               
                <label><strong> Hi, <%=session.getAttribute("uname")%> </strong></label>
                
                 
                <label style="margin-left:45px;"><a href="LogOut"><i class="fa fa-sign-out fa-fw"></i> Logout</a> </label>
                
                </div>

            </nav>
           
            
           
   
                  
                
            <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                      <h1 class="page-header">Welcome to PSP Dashboard</h1>
                </div>
            </div>   
            <!-- /.row -->
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b> Please enter your Assignment details </b>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            
                            <form action="Dashboard_Home_Post" method="post" role="form">
                                
                                
                                
                                <table width="100%" class="table table-striped table-bordered table-hover"  id="Time_Record_Table-example">
                                
                                <tbody>
                                    <tr class="odd gradeX" >
                                        <td>
                                            <label id="Assignment_Name_label">Assignment Name: </label>

                                        </td> 
                                      
                                        <td>
                                             <input type="text" name="Assignment_Name_input" id="Assignment_Name_input" >

                                        </td>
                                        
                                    </tr>
                                    
                                    <tr class="odd gradeX" >
                                        <td>
                                            <label id="Description_label">Description: </label>

                                        </td>
                                      
                                        <td>
                                             <input type="text" name="Description_input" id="Description_input">

                                        </td>
                                        
                                    </tr>
                                    
                                    <tr class="odd gradeX" >
                                        <td>
                                            <label id="assignmentType_label">Assignment Type:  </label>

                                        </td>
                                      
                                        <td>
                                             <select name="assignment_type">
                                                 <option value="PSP 0.0">PSP 0.0</option>    
                                                 <option value="PSP 0.1">PSP 0.1</option>    
                                             </select>    

                                        </td>
                                        
                                    </tr>
                                    
                                     <tr class="odd gradeX" >
                                        <td>
                                            <label id="date_label">Date:  </label>

                                        </td>
                                      
                                        <td>
                                             <input name="Date_input" id="Date_input">

                                        </td>
                                        
                                    </tr>
                                    
                                     
                                    
                                     
                                </tbody>
                                
                                </table>
                                
                                <br/><button id="start_assignment" type="submit" name="Start Assignment" value="Start Assignment" class="btn btn-md btn-success"> Start new assignment </button>
                            
                           
                            </form>
                              
                           
                        </div>
                    <!-- /.panel-body -->
                           

                           
                        </div> <!-- /.panel -->
                       
                    </div> <!-- /.col-lg-12 -->
                    
                </div> <!--row-->
                
                <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Existing Assignments</b>
                        </div>
                        <form action="Existing_Assignments" type="post" role="form">    
                        <div class="panel-body">
                            <select name="exis_assign" class="dash_combo">
                                <c:forEach items="${existingAssignments}" var="databaseValue">
                                    <option value="${databaseValue}">
                                      ${databaseValue}
                                    </option>
                                </c:forEach>
                            </select>    
                        </div>
                        <div id="resume_button_div">
                           <button id="resume_assignment" type="submit" name="Resume Assignment" value="Resume Assignment" class="btn btn-md btn-success"> Resume assignment </button>
                        </div>
                            </form> 
                    </div>
                       
                    
                </div>
             </div>    
                        
                </div> <!--page wrapper-->
           

    
              

    

    <!-- Bootstrap Core JavaScript -->
    <script src="./vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="./vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="./vendor/raphael/raphael.min.js"></script>
    
    <script src="./data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="./dist/js/sb-admin-2.js"></script>
        
        <!-- Flot Charts JavaScript -->
    
    
            

</body>

</html>


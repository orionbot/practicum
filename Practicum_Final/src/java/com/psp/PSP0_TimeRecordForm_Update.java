/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.psp;

import com.mysql.jdbc.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Aswin PC
 */
public class PSP0_TimeRecordForm_Update extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
                
        PrintWriter out = response.getWriter();
        HttpSession session=request.getSession(false);

        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
                HttpSession session=request.getSession(false);
                
                
                if(session!=null)
              {
                  try
                  {
                        Class.forName("com.mysql.jdbc.Driver");
                        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/psptool","root","password");
                        System.out.println("Connected with Database successfully bro!!");
                        
                        String name=(String)session.getAttribute("uname");
                        String assignmentname=session.getAttribute("assignment_name").toString();
                        
                        if(! (request.getParameter("a2").isEmpty()) && (request.getParameter("a2")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET plantime=? where studentname=? AND (assignmentname=? AND phase='Plan')";
                            
                            int plannedTime=Integer.parseInt(request.getParameter("a2"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setInt (1, plannedTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(! (request.getParameter("a3").isEmpty()) && (request.getParameter("a3")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET actual_starttime=? where studentname=? AND (assignmentname=? AND phase='Plan')";
                            
                            float startTime=Float.parseFloat(request.getParameter("a3"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setFloat (1, startTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(! (request.getParameter("a4").isEmpty()) && (request.getParameter("a4")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET actual_stoptime=? where studentname=? AND (assignmentname=? AND phase='Plan')";
                            
                            float stopTime=Float.parseFloat(request.getParameter("a4"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setFloat (1, stopTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(! (request.getParameter("a5").isEmpty()) && (request.getParameter("a5")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET interruptionTime=? where studentname=? AND (assignmentname=? AND phase='Plan')";
                            
                          int interruptionTime=Integer.parseInt(request.getParameter("a5"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setFloat (1, interruptionTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(request.getParameter("a3")!=null && !(request.getParameter("a3").isEmpty()) && request.getParameter("a4")!=null && !(request.getParameter("a4").isEmpty()) && request.getParameter("a5")!=null && !(request.getParameter("a5").isEmpty()))
                        {
                            float startTime=Float.parseFloat(request.getParameter("a3"));
                            float stopTime=Float.parseFloat(request.getParameter("a4"));
                            int interruptionTime=Integer.parseInt(request.getParameter("a5"));
                            int actualTime;
                            
                            if((stopTime-startTime)>=1)
                             actualTime=(int)((stopTime-startTime)*60)-interruptionTime;
                            
                            else
                               actualTime=(int)((stopTime-startTime)*100)-interruptionTime;
                            
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET actualtime=? where studentname=? AND (assignmentname=? AND phase='Plan')";
                            
                            
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setInt (1, actualTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                            
                        }    
                        
                        
                        if(! (request.getParameter("a6").isEmpty()) && (request.getParameter("a6")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET comments=? where studentname=? AND (assignmentname=? AND phase='Plan')";
                            
                            String comments=(String)request.getParameter("a6");
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setString (1, comments);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        
                        if(! (request.getParameter("b2").isEmpty()) && (request.getParameter("b2")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET plantime=? where studentname=? AND (assignmentname=? AND phase='Design')";
                            
                            int plannedTime=Integer.parseInt(request.getParameter("b2"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setInt (1, plannedTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(! (request.getParameter("b3").isEmpty()) && (request.getParameter("b3")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET actual_starttime=? where studentname=? AND (assignmentname=? AND phase='Design')";
                            
                            float startTime=Float.parseFloat(request.getParameter("b3"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setFloat (1, startTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(! (request.getParameter("b4").isEmpty()) && (request.getParameter("b4")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET actual_stoptime=? where studentname=? AND (assignmentname=? AND phase='Design')";
                            
                            float stopTime=Float.parseFloat(request.getParameter("b4"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setFloat (1, stopTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(! (request.getParameter("b5").isEmpty()) && (request.getParameter("b5")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET interruptionTime=? where studentname=? AND (assignmentname=? AND phase='Design')";
                            
                          int interruptionTime=Integer.parseInt(request.getParameter("b5"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setFloat (1, interruptionTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(request.getParameter("b3")!=null && ! (request.getParameter("b3").isEmpty()) && request.getParameter("b4")!=null && ! (request.getParameter("b4").isEmpty()) && request.getParameter("b5")!=null && ! (request.getParameter("b5").isEmpty()))
                        {
                            float startTime=Float.parseFloat(request.getParameter("b3"));
                            float stopTime=Float.parseFloat(request.getParameter("b4"));
                            int interruptionTime=Integer.parseInt(request.getParameter("b5"));
                            int actualTime;
                            
                            if((stopTime-startTime)>=1)
                             actualTime=(int)((stopTime-startTime)*60)-interruptionTime;
                            
                            else
                               actualTime=(int)((stopTime-startTime)*100)-interruptionTime;
                            
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET actualtime=? where studentname=? AND (assignmentname=? AND phase='Design')";
                            
                            
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setInt (1, actualTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                            
                        }    
                        
                        
                        if(! (request.getParameter("b6").isEmpty()) && (request.getParameter("b6")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET comments=? where studentname=? AND (assignmentname=? AND phase='Design')";
                            
                            String comments=(String)request.getParameter("b6");
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setString (1, comments);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        if(! (request.getParameter("c2").isEmpty()) && (request.getParameter("c2")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET plantime=? where studentname=? AND (assignmentname=? AND phase='Code')";
                            
                            int plannedTime=Integer.parseInt(request.getParameter("c2"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setInt (1, plannedTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(! (request.getParameter("c3").isEmpty()) && (request.getParameter("c3")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET actual_starttime=? where studentname=? AND (assignmentname=? AND phase='Code')";
                            
                            float startTime=Float.parseFloat(request.getParameter("c3"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setFloat (1, startTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(! (request.getParameter("c4").isEmpty()) && (request.getParameter("c4")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET actual_stoptime=? where studentname=? AND (assignmentname=? AND phase='Code')";
                            
                            float stopTime=Float.parseFloat(request.getParameter("c4"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setFloat (1, stopTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(! (request.getParameter("c5").isEmpty()) && (request.getParameter("c5")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET interruptionTime=? where studentname=? AND (assignmentname=? AND phase='Code')";
                            
                          int interruptionTime=Integer.parseInt(request.getParameter("c5"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setFloat (1, interruptionTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(request.getParameter("c3")!=null && !(request.getParameter("c3").isEmpty()) && request.getParameter("c4")!=null && !(request.getParameter("c4").isEmpty()) && request.getParameter("c5")!=null && !(request.getParameter("c5").isEmpty()))
                        {
                            float startTime=Float.parseFloat(request.getParameter("c3"));
                            float stopTime=Float.parseFloat(request.getParameter("c4"));
                            int interruptionTime=Integer.parseInt(request.getParameter("c5"));
                           int actualTime;
                            
                            if((stopTime-startTime)>=1)
                             actualTime=(int)((stopTime-startTime)*60)-interruptionTime;
                            
                            else
                               actualTime=(int)((stopTime-startTime)*100)-interruptionTime;
                            
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET actualtime=? where studentname=? AND (assignmentname=? AND phase='Code')";
                            
                            
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setInt (1, actualTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                            
                        }    
                        
                        
                        if(! (request.getParameter("c6").isEmpty()) && (request.getParameter("c6")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET comments=? where studentname=? AND (assignmentname=? AND phase='Code')";
                            
                            String comments=(String)request.getParameter("c6");
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setString (1, comments);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(! (request.getParameter("d2").isEmpty()) && (request.getParameter("d2")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET plantime=? where studentname=? AND (assignmentname=? AND phase='Test')";
                            
                            int plannedTime=Integer.parseInt(request.getParameter("d2"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setInt (1, plannedTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(! (request.getParameter("d3").isEmpty()) && (request.getParameter("d3")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET actual_starttime=? where studentname=? AND (assignmentname=? AND phase='Test')";
                            
                            float startTime=Float.parseFloat(request.getParameter("d3"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setFloat (1, startTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(! (request.getParameter("d4").isEmpty()) && (request.getParameter("d4")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET actual_stoptime=? where studentname=? AND (assignmentname=? AND phase='Test')";
                            
                            float stopTime=Float.parseFloat(request.getParameter("d4"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setFloat (1, stopTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(! (request.getParameter("d5").isEmpty()) && (request.getParameter("d5")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET interruptionTime=? where studentname=? AND (assignmentname=? AND phase='Test')";
                            
                          int interruptionTime=Integer.parseInt(request.getParameter("d5"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setFloat (1, interruptionTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(request.getParameter("d3")!=null && !(request.getParameter("d3").isEmpty()) && request.getParameter("d4")!=null && !(request.getParameter("d4").isEmpty()) && request.getParameter("d5")!=null && !(request.getParameter("d5").isEmpty()))
                        {
                            float startTime=Float.parseFloat(request.getParameter("d3"));
                            float stopTime=Float.parseFloat(request.getParameter("d4"));
                            int interruptionTime=Integer.parseInt(request.getParameter("d5"));
                            int actualTime;
                            
                            if((stopTime-startTime)>=1)
                             actualTime=(int)((stopTime-startTime)*60)-interruptionTime;
                            
                            else
                               actualTime=(int)((stopTime-startTime)*100)-interruptionTime;
                            
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET actualtime=? where studentname=? AND (assignmentname=? AND phase='Test')";
                            
                            
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setInt (1, actualTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                            
                        }    
                        
                        
                        if(! (request.getParameter("d6").isEmpty()) && (request.getParameter("d6")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET comments=? where studentname=? AND (assignmentname=? AND phase='Test')";
                            
                            String comments=(String)request.getParameter("d6");
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setString (1, comments);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(! (request.getParameter("e2").isEmpty()) && (request.getParameter("e2")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET plantime=? where studentname=? AND (assignmentname=? AND phase='PostMortem')";
                            
                            int plannedTime=Integer.parseInt(request.getParameter("e2"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setInt (1, plannedTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(! (request.getParameter("e3").isEmpty()) && (request.getParameter("e3")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET actual_starttime=? where studentname=? AND (assignmentname=? AND phase='PostMortem')";
                            
                            float startTime=Float.parseFloat(request.getParameter("e3"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setFloat (1, startTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(! (request.getParameter("e4").isEmpty()) && (request.getParameter("e4")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET actual_stoptime=? where studentname=? AND (assignmentname=? AND phase='PostMortem')";
                            
                            float stopTime=Float.parseFloat(request.getParameter("e4"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setFloat (1, stopTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(!(request.getParameter("e5").isEmpty()) && (request.getParameter("e5")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET interruptionTime=? where studentname=? AND (assignmentname=? AND phase='PostMortem')";
                            
                          int interruptionTime=Integer.parseInt(request.getParameter("e5"));
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setFloat (1, interruptionTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        if(request.getParameter("e3")!=null && !(request.getParameter("e3").isEmpty()) && request.getParameter("e4")!=null && !(request.getParameter("e4").isEmpty()) && request.getParameter("e5")!=null && !(request.getParameter("e5").isEmpty()))
                        {
                            float startTime=Float.parseFloat(request.getParameter("e3"));
                            float stopTime=Float.parseFloat(request.getParameter("e4"));
                            int interruptionTime=Integer.parseInt(request.getParameter("e5"));
                            
                            int actualTime;
                            
                            if((stopTime-startTime)>=1)
                             actualTime=(int)((stopTime-startTime)*60)-interruptionTime;
                            
                            else
                               actualTime=(int)((stopTime-startTime)*100)-interruptionTime;
                                
                            
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET actualtime=? where studentname=? AND (assignmentname=? AND phase='PostMortem')";
                            
                            
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setInt (1, actualTime);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                            
                        }    
                        
                        
                        if(!(request.getParameter("e6").isEmpty()) && (request.getParameter("e6")!=null))
                        {
                            String updateTableSQL = "UPDATE timerecordform"
		+ " SET comments=? where studentname=? AND (assignmentname=? AND phase='PostMortem')";
                            
                            String comments=(String)request.getParameter("e6");
                            
                        PreparedStatement preparedStatement = con.prepareStatement(updateTableSQL);
                        preparedStatement.setString (1, comments);
                        preparedStatement.setString(2, name);
                        preparedStatement.setString(3, assignmentname);
                        preparedStatement .executeUpdate();
                        System.out.println("Updated a row successfully");
                        
                            
                        }
                        
                        
                            
                            
                                               
                        con.close();
                        
                        /*String insertTableSQL = "INSERT INTO timerecordform"
		+ "(studentname, assignmentname, phase, plantime, actual_starttime,actual_stoptime,actualtime,interruptionTime,comments) VALUES"
		+ "(?,?,?,?,?,?,?,?,?)";
                        
                        String assignmentname=session.getAttribute("assignment_name").toString();
                        
                        
                        
                        JSONArray phase_data = new JSONArray();
                        
                        
                        
                        
                        
                        System.out.println("Parameters for:"+session.getAttribute("uname"));
                        
                        String name=(String)session.getAttribute("uname");
                        String phase=(String)request.getParameter("a1");
                        int plannedTime=Integer.parseInt(request.getParameter("a2"));
                        float startTime=Float.parseFloat(request.getParameter("a3"));
                        float stopTime=Float.parseFloat(request.getParameter("a4"));
                        int interruptionTime=Integer.parseInt(request.getParameter("a5"));
                        int actualTime=(int)((stopTime-startTime)*60)-interruptionTime;
                        
                        String comments=request.getParameter("a6");
                        
                        JSONObject phase1=new JSONObject();
                        phase1.put("phase", phase);
                        phase1.put("plannedTime", plannedTime);
                        phase1.put("actualTime", actualTime);
                        
                        phase_data.put(phase1);
                        
                        
                        System.out.println(phase+" Phase");

                        System.out.println("plannedTime:"+plannedTime);
                        System.out.println("startTime:"+startTime);
                        System.out.println("stopTime:"+stopTime);
                        System.out.println("InterruptionTime:"+interruptionTime);
                        System.out.println("Phase:"+phase);
                        System.out.println("Comments:"+comments);

                        
                        
                        PreparedStatement preparedStatement = con.prepareStatement(insertTableSQL);
                        preparedStatement.setString(1, name);
                        preparedStatement.setString(2, assignmentname);
                        preparedStatement.setString(3, phase);
                        preparedStatement.setInt (4, plannedTime);
                        preparedStatement.setFloat(5, startTime);
                        preparedStatement.setFloat(6, stopTime);
                        preparedStatement.setInt(7, actualTime);
                        preparedStatement.setInt(8, interruptionTime);
                        preparedStatement.setString(9, comments);
                        // execute insert SQL stetement
                        preparedStatement .executeUpdate();
                        
                       
                        
                        System.out.println("Inserted a row successfully");

                        
                        String phase_2=(String)request.getParameter("b1");
                        int plannedTime_2=Integer.parseInt(request.getParameter("b2"));
                        float startTime_2=Float.parseFloat(request.getParameter("b3"));
                        float stopTime_2=Float.parseFloat(request.getParameter("b4"));
                        int interruptionTime_2=Integer.parseInt(request.getParameter("b5"));
                        int actualTime_2=(int)((stopTime_2-startTime_2)*60)-interruptionTime_2;
                        
                        String comments_2=(String)request.getParameter("b6");

                        System.out.println(phase_2+" phase");
                        System.out.println("plannedTime_2:"+plannedTime_2);
                        System.out.println("startTime_2:"+startTime_2);
                        System.out.println("stopTime_2:"+stopTime_2);
                        System.out.println("interruptionTime_2:"+interruptionTime_2);
                        System.out.println("Work Done:"+comments_2);
                        
                        
                        JSONObject phase2=new JSONObject();
                        phase2.put("phase", phase_2);
                        phase2.put("plannedTime", plannedTime_2);
                        phase2.put("actualTime", actualTime_2);
                        
                        phase_data.put(phase2);
                        
                        
                        
                        PreparedStatement preparedStatement2 = con.prepareStatement(insertTableSQL);
                        preparedStatement2.setString(1, name);
                        preparedStatement2.setString(2, assignmentname);
                        preparedStatement2.setString(3, phase_2);
                        preparedStatement2.setInt(4, plannedTime_2);
                        preparedStatement2.setFloat(5, startTime_2);
                        preparedStatement2.setFloat(6, stopTime_2);
                        preparedStatement2.setInt(7, actualTime_2);
                        preparedStatement2.setInt(8, interruptionTime_2);
                        preparedStatement2.setString(9, comments_2);
                        // execute insert SQL stetement
                        preparedStatement2.executeUpdate();

                        System.out.println("Inserted another row successfully");
                        
                        
                        String phase_3=(String)request.getParameter("c1");
                        int plannedTime_3=Integer.parseInt(request.getParameter("c2"));
                        float startTime_3=Float.parseFloat(request.getParameter("c3"));
                        float stopTime_3=Float.parseFloat(request.getParameter("c4"));
                        int interruptionTime_3=Integer.parseInt(request.getParameter("c5"));
                        int actualTime_3=(int)((stopTime_3-startTime_3)*60)-(interruptionTime_3);
                        
                        String comments_3=(String)request.getParameter("c6");

                        System.out.println(phase_3+" phase");
                        System.out.println("plannedTime_3:"+plannedTime_3);
                        System.out.println("startTime_3:"+startTime_3);
                        System.out.println("stopTime_3:"+stopTime_3);
                        System.out.println("interruptionTime_3:"+interruptionTime_3);
                        System.out.println("Work Done:"+comments_3);
                        
                        
                        JSONObject phase3=new JSONObject();
                        phase3.put("phase", phase_3);
                        phase3.put("plannedTime", plannedTime_3);
                        phase3.put("actualTime", actualTime_3);
                        
                        phase_data.put(phase3);
                        
                        
                        
                        PreparedStatement preparedStatement3 = con.prepareStatement(insertTableSQL);
                        preparedStatement3.setString(1, name);
                        preparedStatement3.setString(2, assignmentname);
                        preparedStatement3.setString(3, phase_3);
                        preparedStatement3.setInt(4, plannedTime_3);
                        preparedStatement3.setFloat(5, startTime_3);
                        preparedStatement3.setFloat(6, stopTime_3);
                        preparedStatement3.setInt(7, actualTime_3);
                        preparedStatement3.setInt(8, interruptionTime_3);
                        preparedStatement3.setString(9, comments_3);
                        // execute insert SQL stetement
                        preparedStatement3.executeUpdate();

                        System.out.println("Inserted another row successfully");
                        
           	        String phase_4=(String)request.getParameter("d1");
                        int plannedTime_4=Integer.parseInt(request.getParameter("d2"));
                        float startTime_4=Float.parseFloat(request.getParameter("d3"));
                        float stopTime_4=Float.parseFloat(request.getParameter("d4"));
                        int interruptionTime_4=Integer.parseInt(request.getParameter("d5"));
                        int actualTime_4=(int)((stopTime_4-startTime_4)*60)-interruptionTime_4;
                        
                        String comments_4=(String)request.getParameter("d6");

                        System.out.println(phase_4+" phase");
                        System.out.println("plannedTime_4:"+plannedTime_4);
                        System.out.println("startTime_4:"+startTime_4);
                        System.out.println("stopTime_4:"+stopTime_4);
                        System.out.println("interruptionTime_4:"+interruptionTime_4);
                        System.out.println("Work Done:"+comments_4);
                        
                        
                        JSONObject phase4=new JSONObject();
                        phase4.put("phase", phase_4);
                        phase4.put("plannedTime", plannedTime_4);
                        phase4.put("actualTime", actualTime_4);
                        
                        phase_data.put(phase4);
                        
                        
                        
                        PreparedStatement preparedStatement4 = con.prepareStatement(insertTableSQL);
                        preparedStatement4.setString(1, name);
                        preparedStatement4.setString(2, assignmentname);
                        preparedStatement4.setString(3, phase_4);
                        preparedStatement4.setInt(4, plannedTime_4);
                        preparedStatement4.setFloat(5, startTime_4);
                        preparedStatement4.setFloat(6, stopTime_4);
                        preparedStatement4.setInt(7, actualTime_4);
                        preparedStatement4.setInt(8, interruptionTime_4);
                        preparedStatement4.setString(9, comments_4);
                        // execute insert SQL stetement
                        preparedStatement4.executeUpdate();

                        System.out.println("Inserted another row successfully");

                        String phase_5=(String)request.getParameter("e1");
                        int plannedTime_5=Integer.parseInt(request.getParameter("e2"));
                        float startTime_5=Float.parseFloat(request.getParameter("e3"));
                        float stopTime_5=Float.parseFloat(request.getParameter("e4"));
                        int interruptionTime_5=Integer.parseInt(request.getParameter("e5"));
                        int actualTime_5=(int)((stopTime_5-startTime_5)*60)-interruptionTime_5;
                        
                        String comments_5=(String)request.getParameter("e6");

                        System.out.println(phase_5+" phase");
                        System.out.println("plannedTime_5:"+plannedTime_5);
                        System.out.println("startTime_5:"+startTime_5);
                        System.out.println("stopTime_5:"+stopTime_5);
                        System.out.println("interruptionTime_5:"+interruptionTime_5);
                        System.out.println("Work Done:"+comments_5);
                        
                        
                        JSONObject phase5=new JSONObject();
                        phase5.put("phase", phase_5);
                        phase5.put("plannedTime", plannedTime_5);
                        phase5.put("actualTime", actualTime_5);
                        
                        phase_data.put(phase5);
                        
                        
                        
                        PreparedStatement preparedStatement5 = con.prepareStatement(insertTableSQL);
                        preparedStatement5.setString(1, name);
                        preparedStatement5.setString(2, assignmentname);
                        preparedStatement5.setString(3, phase_5);
                        preparedStatement5.setInt(4, plannedTime_5);
                        preparedStatement5.setFloat(5, startTime_5);
                        preparedStatement5.setFloat(6, stopTime_5);
                        preparedStatement5.setInt(7, actualTime_5);
                        preparedStatement5.setInt(8, interruptionTime_5);
                        preparedStatement5.setString(9, comments_5);
                        // execute insert SQL stetement
                        preparedStatement5.executeUpdate();

                        System.out.println("Inserted another row successfully");
                        
                        String responsetojsp=phase_data.toString();
                        request.setAttribute("mainjson",responsetojsp);
                        request.setAttribute("uname",name);
                            
                                                     
                            System.out.println(responsetojsp);
                            System.out.println("Going to forward request to visualization page");
                            
                            */
                        
                        
               

                       }
                  
                  catch(ClassNotFoundException | NumberFormatException | SQLException ex)
                  {
                      System.out.println(ex.getMessage());
                  }    
               
    }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

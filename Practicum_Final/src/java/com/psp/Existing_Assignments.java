/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.psp;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;
import javax.servlet.RequestDispatcher;

/**
 *
 * @author Aswin PC
 */
public class Existing_Assignments extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Connection con;
        ResultSet rs;
        String selected_assignment = request.getParameter("exis_assign") ;
        String username=new String();
        String description=new String();
        String assignment_type=new String();
        //java.sql.Date dbSqlDate;
        
        Date aDate=new Date();
        
        SimpleDateFormat mFormat = new SimpleDateFormat("dd-MM-yyyy");
        
        HttpSession session=request.getSession(false);
        
     
            
            try{
                
                             
                Class.forName("com.mysql.jdbc.Driver");
                con=DriverManager.getConnection("jdbc:mysql://localhost:3306/psptool","root","");
                System.out.println("Connected with Database successfully!!");
                Statement stmt=con.createStatement();
                
                if(session!=null)
           {
               username=(String)session.getAttribute("uname");
           }
                rs=stmt.executeQuery("select description,assignmenttype,creation_date from assignments where studentname='"+username+"' and assignmentname='"+selected_assignment+"'");
                
                
                while(rs.next())
                {
                    description=rs.getString("description");
                    aDate = rs.getDate("creation_date");
                    assignment_type=rs.getString("assignmenttype");
                }  
                 
                request.setAttribute("assignment_name",selected_assignment);
                session.setAttribute("assignment_name",selected_assignment);
                request.setAttribute("description_input",description);
                request.setAttribute("uname",username);
                request.setAttribute("assignment_type",assignment_type);
                request.setAttribute("date_input",mFormat.format(aDate));
                
                //RequestDispatcher rd= request.getRequestDispatcher("pages/Assignment_Home_Existing.jsp");
                RequestDispatcher rd= request.getRequestDispatcher("pages/Assignment_Home.jsp");
                rd.forward(request,response);
               
          }
    
    catch(Exception ex)
    {
        System.out.println(ex.getMessage());
    }
        
       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

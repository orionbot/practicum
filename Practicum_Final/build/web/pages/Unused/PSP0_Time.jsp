<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Time Log</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">PSP Dashboard</a>
            </div>
            <!-- /.navbar-header -->

            
              
                
                <div style="margin-left:80%;margin-top:1%;">
               
                <label><strong> Hi, <%= session.getAttribute("uname").toString() %> </strong></label>
                
                 
                <label style="margin-left:45px;"><a href="../LogOut"><i class="fa fa-sign-out fa-fw"></i> Logout</a> </label>
                
                </div>
                <!-- /.dropdown -->
            
            <!-- /.navbar-top-links -->

            <!--<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                        
                        <li>
                            <a href="#"><i class="fa fa-file fa-fw"> </i> Forms<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="pages/PSP0_Defect.html">Defect Record</a>
                                </li>
                                <li>
                                    <a href="pages/PSP_LOC_Actual.html">LOC Actual</a>
                                </li>
                                <li>
                                    <a href="pages/PSP_LOC_Planned.html">LOC Planned</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                       <!-- </li>
                        
                        <li>
                            <a href="#"><i class="fa fa-bar-chart fa-fw"></i> Visualisations <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">Planned vs Actual Time</a>
                                </li>
                                <li>
                                    <a href="#">Defects injected per phase</a>
                                </li>
                               
                            </ul>
                           
                        </li>
                        </ul>
                </div>
                <!-- /.sidebar-collapse -->
            <!--</div>-->
            <!-- /.navbar-static-side -->
        </nav>

   
                  
                
                <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                      <h1 class="page-header"><%= session.getAttribute("assignment_name").toString() %>-Time Record Form</h1>
    
                
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Please enter your data.
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            
                            <form action="../PSP0_Time_2" method="post" role="form">
                                
                                
                                
                                <table width="100%" class="table table-striped table-bordered table-hover"  id="Time_Record_Table-example">
                                <thead>
                                    <tr>
                                        <th>Phase</th>
                                        <th>Planned Time (Mins)</th>
                                        <th>Actual Start Time</th>
                                        <th>Actual Stop time</th>
                                        <th>Interruption Time (Mins)</th>
                                        <th>Comments</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX" >
                                        
                                        <td>
                                             <input type="text" name="1a" id="Phase" value="Plan" readonly>

                                        </td>
                                        <td>
                                            <input type="number" name="2a" id="PlannedTime" >

                                        </td> 
                                      
                                        <td>
                                             <input type="number" step="0.01" name="3a" id="Actual_startTime" >

                                        </td>
                                        <td>
                                             <input type="number" step="0.01" name="4a" id="Actual_stopTime">
                                        </td>
                                        
                                        <td>
                                            <input type="number"  name="5a" id="Interruption_time" >
                                        </td>
                                        
                                        <td>
                                            <input type="text" name="6a" id="Comments">
                                        </td>
                                        
                                    </tr>
                                    
                                    <tr class="odd gradeX" >
                                        
                                        <td>
                                             <input type="text" name="1b" id="Phase" value="Design" readonly >

                                        </td>
                                        
                                        <td>
                                            <input type="number" name="2b" id="PlannedTime" >

                                        </td>
                                      
                                        <td>
                                             <input type="number" step="0.01" name="3b" id="Actual_startTime">

                                        </td>
                                        <td>
                                             <input type="number" step="0.01" name="4b" id="Actual_stopTime">
                                        </td>
                                        
                                        <td>
                                            <input type="number"  name="5b" id="Interruption_time">
                                        </td>
                                        
                                        <td>
                                            <input type="text" name="6b" id="Comments">
                                        </td>
                                        
                                    </tr>
                                    
                                     <tr class="odd gradeX" >
                                         
                                         <td>
                                             <input type="text" name="1c" id="Phase" value="Code" readonly>

                                        </td>
                                        
                                        <td>
                                            <input type="number" name="2c" id="PlannedTime" >

                                        </td>
                                      
                                        <td>
                                             <input type="number" step="0.01" name="3c" id="Actual_startTime">

                                        </td>
                                        <td>
                                             <input type=number step="0.01" name="4c" id="Actual_stopTime">
                                        </td>
                                        
                                        <td>
                                             <input type=number  name="5c" id="Interruption_time">
                                        </td>
                                        
                                        <td>
                                            <input type="text" name="6c" id="Comments">
                                        </td>
                                        
                                    </tr>
                                    
                                     <tr class="odd gradeX" >
                                         
                                         <td>
                                             <input type="text" name="1d" id="Phase" value="Test" readonly>

                                        </td>
                                        
                                        <td>
                                            <input type="number" name="2d" id="PlannedTime" >

                                        </td>
                                      
                                        <td>
                                             <input type="number" step="0.01" name="3d" id="Actual_startTime">

                                        </td>
                                        <td>
                                             <input type="number" step="0.01" name="4d" id="Actual_stopTime">
                                        </td>
                                        
                                        <td>
                                             <input type="number"  name="5d" id="Interruption_time">
                                        </td>
                                        
                                        <td>
                                            <input type="text" name="6d" id="Comments">
                                        </td>
                                        
                                    </tr>
                                    
                                    <tr class="odd gradeX" >
                                         
                                         <td>
                                             <input type="text" name="1e" id="Phase" value="PostMortem" readonly>

                                        </td>
                                        
                                        <td>
                                            <input type="number" name="2e" id="PlannedTime" >

                                        </td>
                                      
                                        <td>
                                             <input type="number" step="0.01" name="3e" id="Actual_startTime">

                                        </td>
                                        <td>
                                             <input type="number" step="0.01" name="4e" id="Actual_stopTime">
                                        </td>
                                        
                                        <td>
                                             <input type="number"  name="5e" id="Interruption_time">
                                        </td>
                                        
                                        <td>
                                            <input type="text" name="6e" id="Comments">
                                        </td>
                                        
                                    </tr>
                                </tbody>
                                
                                </table>
                                
                                <br/><button type="submit" name="Submit" value="Submit" class="btn btn-lg btn-success btn-block" style="width:150px;"> Save </button>
                            
                           
                            </form>
                              
                           
                        </div>
               
                           

                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
           

    
              

    
    
    
            </div>
            </div> 
                      
<!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../vendor/raphael/raphael.min.js"></script>
    <script src="../vendor/morrisjs/morris.min.js"></script>
    <script src="../data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
        
        <!-- Flot Charts JavaScript -->
    <script src="../vendor/flot/excanvas.min.js"></script>
    <script src="../vendor/flot/jquery.flot.js"></script>
    <script src="../vendor/flot/jquery.flot.pie.js"></script>
    <script src="../vendor/flot/jquery.flot.resize.js"></script>
    <script src="../vendor/flot/jquery.flot.time.js"></script>
    <script src="../vendor/flot-tooltip/jquery.flot.tooltip.min.js"></script>
    <script src="../data/flot-data.js"></script>                      

</body>

</html>


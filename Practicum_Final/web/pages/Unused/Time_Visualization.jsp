<html>
<head>
<title>Visualization</title>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
   <script src="https://code.highcharts.com/highcharts.js"></script>  
</head>
<body>
<div id="container1" style="width: 550px; height: 400px; margin: 0 auto"></div>
<div id="container2" style="width: 550px; height: 400px; margin: 0 auto"></div>
<script language="JavaScript">
$(document).ready(function() { 
    
    var foo=JSON.parse('${mainjson}');
   var chart = {
       plotBackgroundColor: null,
       plotBorderWidth: null,
       plotShadow: false
   };
   var title = {
      text: 'Planned Time for different phases'   
   };      
   var tooltip = {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
   };
   var plotOptions = {
      pie: {
         allowPointSelect: true,
         cursor: 'pointer',
         dataLabels: {
            enabled: false           
         },
         showInLegend: true
      }
   };
   var series= [{
      type: 'pie',
      name: 'Time share',
      data: [
         ['Plan',   foo[0].plannedTime],
         ['Design', foo[1].plannedTime],
         {
            name: 'Code',
            y: foo[2].plannedTime,
            sliced: true,
            selected: true
         },
         ['Test',    foo[3].plannedTime]
         
      ]
   }];     
      
   var json = {};   
   json.chart = chart; 
   json.title = title;     
   json.tooltip = tooltip;  
   json.series = series;
   json.plotOptions = plotOptions;
   
   var chart_2 = {
       plotBackgroundColor: null,
       plotBorderWidth: null,
       plotShadow: false
   };
   var title_2 = {
      text: 'Actual Time for different phases'   
   };      
   
   var plotOptions_2 = {
      pie: {
         allowPointSelect: true,
         cursor: 'pointer',
         dataLabels: {
            enabled: false           
         },
         showInLegend: true
      }
   };
   var series_2= [{
      type: 'pie',
      name: 'Time share',
      data: [
         ['Plan',   foo[0].actualTime],
         ['Design', foo[1].actualTime],
         {
            name: 'Code',
            y: foo[2].actualTime,
            sliced: true,
            selected: true
         },
         ['Test',    foo[3].actualTime]
         
      ]
   }];     
      
   var json_2 = {};   
   json_2.chart = chart_2; 
   json_2.title = title_2;     
   json_2.tooltip = tooltip;  
   json_2.series = series_2;
   json_2.plotOptions = plotOptions_2;
   
   
   $('#container1').highcharts(json); 
   $('#container2').highcharts(json_2); 
});
</script>
</body>
</html>
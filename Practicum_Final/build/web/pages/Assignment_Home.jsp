<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Assignment Home</title>
    
    <link href="./dist/css/Assignment_Home.css" rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    <link href="./vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    

    <!-- MetisMenu CSS -->
    <link href="./vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="./vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="./vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
      

</head>

<body>

    

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="Dashboard_Home_Pre">PSP Dashboard</a>
                
                
            </div>
            <!-- /.navbar-header -->
            
            <div id="username_div">
               
                <label><strong> Hi, <%=request.getAttribute("uname")%> </strong></label>
                
                 
                <label id="logout_label"><a href="LogOut"><i class="fa fa-sign-out fa-fw"></i> Logout</a> </label>
                
                </div>

            </nav>
                <!-- /.dropdown -->
            
            <!-- /.navbar-top-links -->

            <!--<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                        
                        <li>
                            <a href="#"><i class="fa fa-file fa-fw"> </i> Assignments<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="pages/PSP0_Defect.html">Defect Record</a>
                                </li>
                                <li>
                                    <a href="pages/PSP_LOC_Actual.html">LOC Actual</a>
                                </li>
                                <li>
                                    <a href="pages/PSP_LOC_Planned.html">LOC Planned</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        <!--</li>-->
                        
                        <!--<li>
                            <a href="#"><i class="fa fa-bar-chart fa-fw"></i> Visualisations <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">Planned vs Actual Time</a>
                                </li>
                                <li>
                                    <a href="#">Defects injected per phase</a>
                                </li>
                               
                            </ul>
                           
                        </li>
                        </ul>
                </div>-->
                <!-- /.sidebar-collapse -->
            <!--</div>-->
            <!-- /.navbar-static-side -->
        

   
                  
                
                <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                      <h1 class="page-header">Assignment</h1>
    
                
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            
                            <table width="100%" class="table table-striped table-bordered table-hover"  id="Time_Record_Table-example">
                                
                                <tbody>
                                    <tr class="odd gradeX" >
                                        <td>
                                            <label id="Assignment_Name_label">Assignment Name: </label>

                                        </td> 
                                      
                                        <td>
                                             <%=request.getAttribute("assignment_name")%>
                                             
                                             
                                             

                                        </td>
                                        
                                    </tr>
                                    
                                    <tr class="odd gradeX" >
                                        <td>
                                            <label id="Description_label">Description: </label>

                                        </td>
                                      
                                        <td>
                                             <%=request.getAttribute("description_input")%>

                                        </td>
                                        
                                    </tr>
                                    
                                     <tr class="odd gradeX" >
                                        <td>
                                            <label id="assignmentType_label">Assignment Type: </label>

                                        </td>
                                      
                                        <td>
                                             <%=request.getAttribute("assignment_type")%>

                                        </td>
                                        
                                    </tr>
                                    
                                    
                                    <tr class="odd gradeX" >
                                        <td>
                                            <label id="Description_label">Created on: </label>

                                        </td>
                                      
                                        <td>
                                             <%=request.getAttribute("date_input")%>

                                        </td>
                                        
                                    </tr>
                                    
                                   
                                    
                                </tbody>
                                
                                </table>
                                
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            
                                                  
                                <table width="100%" class="table table-striped table-bordered table-hover"  id="Time_Record_Table-example">
                                <thead>
                                    <tr>
                                        <th>Forms</th>
                                    </tr>
                                </thead>    
                                <tbody>
                                    <tr class="odd gradeX" >
                                        <td>
                                            <label id="time_record"><a href="PSP0_Time_Fetch">Time Recording Log Form </a> </label>

                                        </td> 
                                      
                                        
                                    </tr>
                                    
                                    <tr class="odd gradeX" >
                                        <td>
                                            <label id="defect_record">Defect Recording Log Form</label>

                                        </td>
                                      
                                        
                                    </tr>
                                    
                                    <tr class="odd gradeX" >
                                        <td>
                                            <label id="test_report">Test Report Form </label>

                                        </td>
                                      
                                        
                                    </tr>
                                    
                                    <tr class="odd gradeX" >
                                        <td>
                                            <label id="project_plan_summary">Project Plan Summary Form </label>

                                        </td>
                                      
                                        
                                    </tr>
                                    
                                     
                                </tbody>
                                
                                </table>
                                
                            
                            
                           
                            
                              
                           
                        </div>
               
                            <!-- /.table-responsive -->
                             <!--<br/> <a href=""> <button  type="button" class="btn btn-warning" style="margin-left:15px;">Go to Defect Form</button></a>-->

                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
           

    
              

    <!-- jQuery -->
    <script src="./vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="./vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="./vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="./vendor/raphael/raphael.min.js"></script>
    <script src="./vendor/morrisjs/morris.min.js"></script>
    <script src="./data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="./dist/js/sb-admin-2.js"></script>
        
        <!-- Flot Charts JavaScript -->
    <script src="./vendor/flot/excanvas.min.js"></script>
    <script src="./vendor/flot/jquery.flot.js"></script>
    <script src="./vendor/flot/jquery.flot.pie.js"></script>
    <script src="./vendor/flot/jquery.flot.resize.js"></script>
    <script src="./vendor/flot/jquery.flot.time.js"></script>
    <script src="./vendor/flot-tooltip/jquery.flot.tooltip.min.js"></script>
    <script src="./data/flot-data.js"></script>
    
    
            </div>

</body>

</html>


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.psp;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Aswin PC
 */
public class PSP0_TimeRecordForm_Get extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
         response.setContentType("text/html;charset=UTF-8");
        
        Connection con;
        ResultSet rs;
        DecimalFormat df=new DecimalFormat("#.##");
        
        
        try{
       
        Class.forName("com.mysql.jdbc.Driver");
        con=DriverManager.getConnection("jdbc:mysql://localhost:3306/psptool","root","");
        System.out.println("Connected with Database successfully!!");
        Statement stmt=con.createStatement();
        HttpSession session=request.getSession(false);
        String username=new String();

        String assignmentname=(String)session.getAttribute("assignment_name");
        
        System.out.println("Assignment name:"+assignmentname);
        
            if(session!=null)
           {
               username=(String)session.getAttribute("uname");
           }
            
        rs=stmt.executeQuery("select plantime,actual_starttime,actual_stoptime,interruptionTime,comments from timerecordform where studentname='"+username+"' and assignmentname='"+assignmentname+"' and phase='Plan'");
        
        while(rs.next())
        {   
            if(rs.getInt("plantime")!=0)
            {
                int plantime=rs.getInt("plantime");
                request.setAttribute("Plan_plantime",plantime);
            }
            
            if(rs.getFloat("actual_starttime")!=0.0f)
            {
                
                BigDecimal starttime=rs.getBigDecimal("actual_starttime");
                
                request.setAttribute("Plan_starttime",starttime);
            }
            
            if(rs.getFloat("actual_stoptime")!=0.0f)
            {
                BigDecimal stoptime=rs.getBigDecimal("actual_stoptime");
                request.setAttribute("Plan_stoptime",stoptime);
            }
            
            if(rs.getInt("interruptionTime")!=0)
            {
                int interruptionTime=rs.getInt("interruptionTime");
                request.setAttribute("Plan_interruptionTime",interruptionTime);
            }
            
            if(rs.getString("comments")!=null)
            {
                String comments=rs.getString("comments");
                request.setAttribute("Plan_comments",comments);
            }    
            
        }
        
        
        
        // Fetching Values for Design Phase
        
        rs=stmt.executeQuery("select plantime,actual_starttime,actual_stoptime,interruptionTime,comments from timerecordform where studentname='"+username+"' and assignmentname='"+assignmentname+"' and phase='Design'");
        
        while(rs.next())
        {   
            if(rs.getInt("plantime")!=0)
            {
                int plantime=rs.getInt("plantime");
                request.setAttribute("Design_plantime",plantime);
            }
            
            if(rs.getFloat("actual_starttime")!=0.0f)
            {
                BigDecimal starttime=rs.getBigDecimal("actual_starttime");
                request.setAttribute("Design_starttime",starttime);
            }
            
            if(rs.getFloat("actual_stoptime")!=0.0f)
            {
                float stoptime=rs.getFloat("actual_stoptime");
                request.setAttribute("Design_stoptime",stoptime);
            }
            
            if(rs.getInt("interruptionTime")!=0)
            {
                int interruptionTime=rs.getInt("interruptionTime");
                request.setAttribute("Design_interruptionTime",interruptionTime);
            }
            
            if(rs.getString("comments")!=null)
            {
                String comments=rs.getString("comments");
                request.setAttribute("Design_comments",comments);
            }    
            
        }
        
        rs=stmt.executeQuery("select plantime,actual_starttime,actual_stoptime,interruptionTime,comments from timerecordform where studentname='"+username+"' and assignmentname='"+assignmentname+"' and phase='Code'");
        
        while(rs.next())
        {   
            if(rs.getInt("plantime")!=0)
            {
                int plantime=rs.getInt("plantime");
                request.setAttribute("Code_plantime",plantime);
            }
            
            if(rs.getFloat("actual_starttime")!=0.0f)
            {
                BigDecimal starttime=rs.getBigDecimal("actual_starttime");
                request.setAttribute("Code_starttime",starttime);
            }
            
            if(rs.getFloat("actual_stoptime")!=0.0f)
            {
                BigDecimal stoptime=rs.getBigDecimal("actual_stoptime");
                request.setAttribute("Code_stoptime",stoptime);
            }
            
            if(rs.getInt("interruptionTime")!=0)
            {
                int interruptionTime=rs.getInt("interruptionTime");
                request.setAttribute("Code_interruptionTime",interruptionTime);
            }
            
            if(rs.getString("comments")!=null)
            {
                String comments=rs.getString("comments");
                request.setAttribute("Code_comments",comments);
            }    
            
        }
        
        rs=stmt.executeQuery("select plantime,actual_starttime,actual_stoptime,interruptionTime,comments from timerecordform where studentname='"+username+"' and assignmentname='"+assignmentname+"' and phase='Test'");
        
        while(rs.next())
        {   
            if(rs.getInt("plantime")!=0)
            {
                int plantime=rs.getInt("plantime");
                request.setAttribute("Test_plantime",plantime);
            }
            
            if(rs.getFloat("actual_starttime")!=0.0f)
            {
                BigDecimal starttime=rs.getBigDecimal("actual_starttime");
                request.setAttribute("Test_starttime",starttime);
            }
            
            if(rs.getFloat("actual_stoptime")!=0.0f)
            {
                float stoptime=rs.getFloat("actual_stoptime");
                request.setAttribute("Test_stoptime",stoptime);
            }
            
            if(rs.getInt("interruptionTime")!=0)
            {
                int interruptionTime=rs.getInt("interruptionTime");
                request.setAttribute("Test_interruptionTime",interruptionTime);
            }
            
            if(rs.getString("comments")!=null)
            {
                String comments=rs.getString("comments");
                request.setAttribute("Test_comments",comments);
            }    
            
        }
        
        rs=stmt.executeQuery("select plantime,actual_starttime,actual_stoptime,interruptionTime,comments from timerecordform where studentname='"+username+"' and assignmentname='"+assignmentname+"' and phase='PostMortem'");
        
        while(rs.next())
        {   
            if(rs.getInt("plantime")!=0)
            {
                int plantime=rs.getInt("plantime");
                request.setAttribute("PostMortem_plantime",plantime);
            }
            
            if(rs.getFloat("actual_starttime")!=0.0f)
            {
                BigDecimal starttime=rs.getBigDecimal("actual_starttime");
                                
                request.setAttribute("PostMortem_starttime",starttime);
            }
            
            if(rs.getFloat("actual_stoptime")!=0.0f)
            {
                BigDecimal stoptime=rs.getBigDecimal("actual_stoptime");
                request.setAttribute("PostMortem_stoptime",stoptime);
            }
            
            if(rs.getInt("interruptionTime")!=0)
            {
                int interruptionTime=rs.getInt("interruptionTime");
                request.setAttribute("PostMortem_interruptionTime",interruptionTime);
            }
            
            if(rs.getString("comments")!=null)
            {
                String comments=rs.getString("comments");
                request.setAttribute("PostMortem_comments",comments);
            }    
            
        }
        
        
        
        RequestDispatcher rd= request.getRequestDispatcher("pages/PSP0_TimeRecordForm.jsp");
         
        System.out.println("Executed till this point");
        rd.forward(request,response);
         
        
        /*JSONArray student_data = new JSONArray();
        
        
        while(rs.next())
        {            
           JSONObject student=new JSONObject();
           student.put("phase",rs.getString("phase") );
           student.put("plannedTime", rs.getInt("plantime"));
           student.put("actual_startTime", rs.getFloat("actual_starttime"));
           student.put("actual_stopTime", rs.getFloat("actual_stoptime"));
           student.put("interruptionTime", rs.getFloat("interruptionTime"));
           student.put("comments", rs.getString("comments"));
           student_data.put(student);
           
        }  
                        
         String responsetojsp=student_data.toString();
         request.setAttribute("student_data",responsetojsp);
         
         System.out.println("Student Data:"+responsetojsp);*/
         
         
         
         System.out.println("No Exception till this point");
            
        }
         catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }  
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

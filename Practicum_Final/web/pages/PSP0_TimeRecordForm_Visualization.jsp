<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Visualization Page</title>
    
    <!-- JSTL TAGS-->
    <%@ page import="java.io.*,java.util.*,java.sql.*"%>
    <%@ page import="javax.servlet.http.*,javax.servlet.*" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
    

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    

    
    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2_duplicate.css" rel="stylesheet">

    
    
    <link href="../dist/css/PSP0_TRV.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
     <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

   
    
    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
        
       
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    
    

</head>

<style>
    .chart_div
    {
        width:550px;
        height:400px;
        margin-left:200px;
    }
    
    #PWChartHolder
    {
        display:none;
    }
    
    #CompChartHolder
    {
        display:none;
    }
    
    #container1
    {
        display:none;
    }
    
    #container2
    {
        display:none;
    }
    
    #container3
    {
        display:none;
    }
    
    
</style>

<script>
    
    $(document).ready(function() {
      $("#exis_assign").on('change',(function(e) {
          
        e.preventDefault();
        
        var $assignment=document.getElementById('exis_assign').value
        
        $.get('../PSP0_TimeRecord_Visualize',{assignment:$assignment},function(responseJSON){
            
             if(responseJSON.localeCompare("The form for the Assignment has not been filled yet!")!==0)
             {
                 
                 
                 document.getElementById('Chart_text').innerHTML='The Visualization for the form has been displayed below';
                 
                 document.getElementById('CompChartHolder').style.display = "block";
                document.getElementById('PWChartHolder').style.display = "block";
                document.getElementById('container1').style.display = "block";
                document.getElementById('container2').style.display = "block";
                document.getElementById('container3').style.display = "block";
                 
                 var foo=JSON.parse(responseJSON);
   var chart = {
       plotBackgroundColor: null,
       plotBorderWidth: null,
       plotShadow: false
   };
   var title = {
      text: 'Planned Time for different phases'   
   };      
   var tooltip = {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
   };
   var plotOptions = {
      pie: {
         allowPointSelect: true,
         cursor: 'pointer',
         dataLabels: {
            enabled: false           
         },
         showInLegend: true
      }
   };
   var series= [{
      type: 'pie',
      name: 'Time share',
      data: [
         ['Plan',   foo[0].plannedTime],
         ['Design', foo[1].plannedTime],
         {
            name: 'Code',
            y: foo[2].plannedTime,
            sliced: true,
            selected: true
         },
         ['Test',    foo[3].plannedTime]
         
      ]
   }];     
      
   var json = {};   
   json.chart = chart; 
   json.title = title;     
   json.tooltip = tooltip;  
   json.series = series;
   json.plotOptions = plotOptions;
   
   var chart_2 = {
       plotBackgroundColor: null,
       plotBorderWidth: null,
       plotShadow: false
   };
   var title_2 = {
      text: 'Actual Time for different phases'   
   };      
   
   var plotOptions_2 = {
      pie: {
         allowPointSelect: true,
         cursor: 'pointer',
         dataLabels: {
            enabled: false           
         },
         showInLegend: true
      }
   };
   var series_2= [{
      type: 'pie',
      name: 'Time share',
      data: [
         ['Plan',   foo[0].actualTime],
         ['Design', foo[1].actualTime],
         {
            name: 'Code',
            y: foo[2].actualTime,
            sliced: true,
            selected: true
         },
         ['Test',    foo[3].actualTime]
         
      ]
   }];     
      
   var json_2 = {};   
   json_2.chart = chart_2; 
   json_2.title = title_2;     
   json_2.tooltip = tooltip;  
   json_2.series = series_2;
   json_2.plotOptions = plotOptions_2;
   
   $('#container1').highcharts(json); 
   $('#container2').highcharts(json_2); 
   
   
    Highcharts.chart('container3', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                'Plan',
                'Design',
                'Code',
                'Test'
                
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Time (Minutes)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mins</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                pointWidth:30
            }
                   
        },
        series: [{
            name: 'Plan Time',
            data: [foo[0].plannedTime, foo[1].plannedTime, foo[2].plannedTime, foo[3].plannedTime]

        }, {
            name: 'Actual Time',
            data: [foo[0].actualTime, foo[1].actualTime, foo[2].actualTime, foo[3].actualTime]

        }]
    });
             }    
             
             else
             {
                document.getElementById('Chart_text').innerHTML=responseJSON;
                document.getElementById('CompChartHolder').style.display = "none";
                document.getElementById('PWChartHolder').style.display = "none";
                document.getElementById('container1').style.display = "none";
                document.getElementById('container2').style.display = "none";
                document.getElementById('container3').style.display = "none";
             }  
        });
        
         
    }));
   
       
});


    
</script>    


<body>
    
     <sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
     url="jdbc:mysql://localhost:3306/psptool"
     user="root"  password="password"/>
     
     <sql:query dataSource="${snapshot}" var="result">
        SELECT assignmentname from assignments where studentname='${sessionScope.uname}';
     </sql:query>
        
        
     
     

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                
                <a class="navbar-brand" href="../Dashboard_Home_Pre">PSP Dashboard</a>
                               
                
            </div>
            
            <div id="usernamediv">
               
                <label><strong> Hi, <%=session.getAttribute("uname")%> </strong></label>
                
                 
                <label id="LogOutLabel"><a href="../LogOut"><i class="fa fa-sign-out fa-fw"></i> Logout</a> </label>
                
                </div>
            <!-- /.navbar-header -->

            
                
            </nav>
            <!-- /.navbar-top-links -->

           
              
                <div id="page-wrapper">
                    
                    
                  
                    <div class="col-md-12">
                        
                        
                  <div id="comboBoxHolder" class="row"> 
                      <label id="assignmentSelect_label"><b> Please select the Assignment to view Visualization  </b> </label>  
                      
                      
                      <select id="exis_assign" name="exis_assign" class="dash_combo">
                                <c:forEach items="${result.rows}" var="databaseValue">
                                    <option value="${databaseValue.assignmentname}">
                                      ${databaseValue.assignmentname}
                                    </option>
                                </c:forEach>
                            </select> 
                          
                          
                  </div>  
                        
                        <div  id="LabelHolder" class="row">
                            <label id="Chart_text"> <h2>No Assignment has been selected yet! </h2></label>
                        
                        </div>
                       
                    </div>
                    
                    <div class="row" id="PWChartHolder">
                <div class="col-lg-12">
                    <center><h4 class="page-header">Planned Vs Actual Visualization</h4></center>
    
                
                 </div>
            <!-- /.row -->
            
       <div class="row">
           <div class="col-md-6" id="container1" class="chart_div">
                   
                </div>
                
                 <div class="col-md-6" id="container2" class="chart_div">
                   
                </div>
                    
                </div>
            
            <div class="col-lg-12" id="CompChartHolder">
                <center><h6 class="page-header">Planned Vs Actual Comparison</h6> </center>
            </div>
            
            <div class="col-md-12" id="container3" class="chart_div">
                
            </div>
                </div>
                    
                    
           
    </div> <!--Page Wrapper-->
    
    </div> <!--Wrapper-->
</body>

</html>

